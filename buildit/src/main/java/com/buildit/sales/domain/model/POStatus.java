package com.buildit.sales.domain.model;

public enum POStatus {
	PENDING, OPEN, REJECTED, CLOSED
}
